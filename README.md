# README #

# Bowling Score Program

Program that will read a file with player score shots of bowling. Program should calculate scores and print
scores.

## Getting Started 

### Prerequisites

* Java 8
* Gradle

### How do I get set up? ###
* Clone Repository

```
git clone https://bebefabian@bitbucket.org/bebefabian/bowling.git
```

* Move to project directory
```
cd /path/to-your-project-dir/
```

* Install Project
```
./gradlew build
```
This will run all the tests of the program.

* Run Program
```
java -jar build/libs/challenge-1.0-SNAPSHOT.jar -file=path/to/file.txt

```
### Examples Files ?###
```
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
Jose 10
```
