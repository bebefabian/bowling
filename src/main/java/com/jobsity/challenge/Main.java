package com.jobsity.challenge;
import com.jobsity.challenge.game.BowlingGameHandler;

import java.io.File;
public class Main {


    public static void exit(String message){
        System.out.println(message);
        System.exit(1);
    }

    public static void main(String[] args) {

        if(args.length == 0)
            exit("No parameters");

        String param = args[0];
        String fileName = null;
        if(param.startsWith("-file=")){
            fileName = param.substring("-file=".length());
            File file = new File(fileName);
            if(!file.exists()){
                exit("File does not exists.");
            }

        }else {
            exit("Invalid param option. Try with -file=");
        }

        new BowlingGameHandler().init(fileName);

    }
}
