package com.jobsity.challenge.game;

import com.jobsity.challenge.game.framehandler.BowlingFrameHandler;
import com.jobsity.challenge.game.framehandler.FrameHandler;
import com.jobsity.challenge.model.Frame;
import com.jobsity.challenge.model.Player;
import com.jobsity.challenge.util.PrintUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;


public class BowlingGame implements Game {

    private Map<String, List<Frame>> playerListMap;
    private FrameHandler frameHandler;


    @Override
    public void addScore(String playerName, String score) {
        List<Frame> frames = playerListMap.get(playerName);
        if(frames == null){
            frames = new ArrayList<>();
        }
        frames = frameHandler.addShot(frames,score);
        playerListMap.put(playerName,frames);

    }

    @Override
    public void print() {
        System.out.print("Frame"+"\t"+"\t"+"\t");
        for(int i=1;i<11;i++){
            System.out.print(i + "\t"+"\t"+"\t");
        }
        System.out.println();
        this.playerListMap.forEach((k,v)->{
            System.out.println(k);
            System.out.print("Pintfalls");
            v.stream().forEach(frame1 -> {
                if(frame1.getNumber() == 9){

                    System.out.print("\t\t"+frame1.getFirstShotStr() + "\t\t" + frame1.getSecondShotStr() + "\t\t" + frame1.getThirdShotStr()==null?"":frame1.getThirdShotStr());

                }else if(frame1.isStrike()){
                    System.out.print("\t\t"+"x");

                }else if(frame1.isSpare()){
                    System.out.print("\t\t"+frame1.getFirstShotStr() + "\t" + "/");
                }else {
                    System.out.print("\t\t"+frame1.getFirstShotStr() + "\t\t" + frame1.getSecondShotStr());
                }
            });

            System.out.println();
            System.out.print("Score"+"\t"+"\t"+"\t");

            v.stream().forEach(frame1 -> {
                System.out.print(frame1.getScore() + "\t"+"\t"+"\t");

            });

        });



        System.out.println();
    //    System.out.println();
        System.out.println();

    }

    @Override
    public void startGame() {
        this.playerListMap = new HashMap<>();
        this.frameHandler = new BowlingFrameHandler();
    }

    @Override
    public void computeScore() {
        this.playerListMap.forEach((k,v)->frameHandler.computeScore(v));
    }

    @Override
    public List<Frame> getFramesByPlayer(String player) {
        return this.playerListMap.get(player);
    }

}
