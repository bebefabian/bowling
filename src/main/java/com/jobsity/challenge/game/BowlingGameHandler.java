package com.jobsity.challenge.game;

import com.jobsity.challenge.dto.PlayerInputScore;
import com.jobsity.challenge.reader.BowlingFileReader;
import com.jobsity.challenge.reader.GameFileReader;

import java.util.List;

public class BowlingGameHandler implements GameHandler {

    private GameFileReader fileReader;
    private Game game;

    @Override
    public void init(String fileInput) {
        game.startGame();

        List<PlayerInputScore> playerInputScores = fileReader.read(fileInput);

        playerInputScores.stream().forEach(p -> {
            game.addScore(p.getName(),p.getScore());

        });

        game.computeScore();
        game.print();
    }

    public BowlingGameHandler(){
        this.fileReader = new BowlingFileReader();
        this.game = new BowlingGame();
    }
}
