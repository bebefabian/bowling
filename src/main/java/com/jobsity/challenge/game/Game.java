package com.jobsity.challenge.game;

import com.jobsity.challenge.model.Frame;

import java.util.List;

public interface Game {


     void addScore(String player, String score);
     void print();
     void startGame();
     void computeScore();
     List<Frame> getFramesByPlayer(String player);

}
