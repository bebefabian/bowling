package com.jobsity.challenge.game.framehandler;

import com.jobsity.challenge.model.Frame;

import java.util.List;

public class BowlingFrameHandler implements FrameHandler {


    @Override
    public List<Frame> computeScore(List<Frame> frames) {
        int length = frames.size();
        Frame actualFrame;
        Integer actualScore = 0;
        for (int i = 0; i < length; i++) {

            actualFrame = frames.get(i);


            if (actualFrame.isStrike()) {
                if(actualFrame.getNumber()==9){
                    actualFrame.setScore(actualScore + actualFrame.getFirstShot() + actualFrame.getSecondShot() + actualFrame.getThirdShot());
                    actualScore = actualFrame.getScore();
                }
                else if ((length - 1) > i) {
                    Frame nextFrame = frames.get(i + 1);
                    if (nextFrame.isSpare()) {
                        actualFrame.computeScore(actualScore + 10);
                        actualScore = actualFrame.getScore();
                    } else if (nextFrame.isStrike()) {
                        if(nextFrame.getNumber() == 9 && nextFrame.isDone()){
                            actualFrame.setScore(actualScore + nextFrame.getFirstShot() + nextFrame.getSecondShot() + nextFrame.getThirdShot());
                            actualScore = actualFrame.getScore();
                        }else{
                            if ((length - 1) > i + 1) {
                                Frame nextNextFrame = frames.get(i + 2);
                                actualFrame.computeScore(actualScore + nextFrame.getFirstShot() + nextNextFrame.getFirstShot());
                                actualScore = actualFrame.getScore();
                            }
                        }


                    } else {
                        if (nextFrame.getFirstShot() != null && nextFrame.getSecondShot() != null) {
                            actualFrame.computeScore(actualScore + nextFrame.getFirstShot() + nextFrame.getSecondShot());
                            actualScore = actualFrame.getScore();
                        }
                    }
                }

            } else if (actualFrame.isSpare()) {

                if ((length - 1) > i) {
                    Frame nextFrame = frames.get(i + 1);
                    if (nextFrame.isStrike()) {
                        actualFrame.computeScore(actualScore + 10);
                    } else if (nextFrame.getFirstShot() != null) {
                        actualFrame.computeScore(actualScore + nextFrame.getFirstShot());
                        actualScore = actualFrame.getScore();

                    }
                }

            } else {
                if (actualFrame.getScore() == null) {
                    if (actualFrame.getFirstShot() != null && actualFrame.getSecondShot() != null) {
                        actualFrame.computeScore(actualScore);
                        actualScore = actualFrame.getScore();
                    }
                } else {
                    actualScore = actualFrame.getScore();
                }
            }
        }
        return frames;


    }

    @Override
    public List<Frame> addShot(List<Frame> frames, String shot) {
        if(frames.stream().filter(c -> c.isDone()).count() == Frame.LIMIT_FRAME){
            return frames;
        }
        int size = frames.size();
        Frame frame = null;


        if(size == 0){

            frame = new Frame(0);
            frame.addShot(shot);
            frames.add(frame);

        }else {
            Frame lastFrame = frames.get(size - 1);
                if (lastFrame.isDone()) {
                    frame = new Frame(size);
                    frame.addShot(shot);
                    frames.add(frame);

                } else {
                    lastFrame.addShot(shot);
                }


            }

        return frames;
    }
}
