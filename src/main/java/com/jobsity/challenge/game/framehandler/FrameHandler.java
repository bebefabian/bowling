package com.jobsity.challenge.game.framehandler;

import com.jobsity.challenge.model.Frame;

import java.util.List;

public interface FrameHandler {

     List<Frame> computeScore(List<Frame> frames);
     List<Frame> addShot(List<Frame> frames,String shot);

}
