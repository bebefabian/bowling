package com.jobsity.challenge.model;

public class Frame {

    private static final String FAULT = "F";
    public static final Long LIMIT_FRAME = new Long(10);

    private Integer firstShot;
    private Integer secondShot;
    private Integer thirdShot;
    private String thirdShotStr;
    private Integer score;
    private int number;
    private boolean strike;
    private boolean spare;
    private String firstShotStr;
    private String secondShotStr;

    public Integer getThirdShot() {
        if(thirdShot==null)
            return 0;
        return thirdShot;
    }

    public void setThirdShot(Integer thirdShot) {
        this.thirdShot = thirdShot;
    }

    public String getThirdShotStr() {
        return thirdShotStr;
    }

    public void setThirdShotStr(String thirdShotStr) {
        this.thirdShotStr = thirdShotStr;
    }

    public Frame(int number) {
        this.number = number;
    }

    public Integer getFirstShot() {
        return firstShot;
    }

    public void setFirstShot(Integer firstShot) {
        this.firstShot = firstShot;
    }

    public Integer getSecondShot() {
        return secondShot;
    }

    public void setSecondShot(Integer secondShot) {
        this.secondShot = secondShot;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isStrike() {
        return strike;
    }

    public void setStrike(boolean strike) {
        this.strike = strike;
    }

    public boolean isSpare() {
        return spare;
    }

    public void setSpare(boolean spare) {
        this.spare = spare;
    }

    public String getFirstShotStr() {
        return firstShotStr;
    }

    public void setFirstShotStr(String firstShotStr) {
        this.firstShotStr = firstShotStr;
    }

    public String getSecondShotStr() {
        return secondShotStr;
    }

    public void setSecondShotStr(String secondShotStr) {
        this.secondShotStr = secondShotStr;
    }

    public void addShot(String shotStr) {
        Integer shot = null;
        if (!FAULT.equals(shotStr)) {
            shot = Integer.parseInt(shotStr);
        } else {
            shot = 0;
        }

        if (shot < 0 || shot > 10) {
            throw new RuntimeException("Invalid Input");
        }

        //Calculate if is 10th frame
        if(number == 9 && isStrike()){
            if(secondShot == null){
                setSecondShotStr(shotStr);
                setSecondShot(shot);
                return;
            }else {
                setThirdShot(shot);
                setThirdShotStr(shotStr);
                return;

            }
        }
        //Only exception
//        if (number == 9 && isStrike()) {
//            if(secondShot == null){
//                setSecondShotStr(shotStr);
//                setSecondShot(shot);
//            }else {
//                setThirdShot(shot);
//                setSecondShotStr(shotStr);
//
//            }
//
//        } else {

            if (firstShot == null) {

                setFirstShot(shot);
                setFirstShotStr(shotStr);
                if (firstShot == 10) {
                    setStrike(true);
                }


            } else if (secondShot == null) {
                if (firstShot + shot > 10) {
                    throw new RuntimeException("Invalid Input");
                }
                setSecondShot(shot);
                setSecondShotStr(shotStr);
                if (firstShot + secondShot == 10) {
                    setSpare(true);
                }
            }

        }


  //  }

    private boolean shotsFull() {
        return firstShot != null && secondShot != null;
    }

    public boolean isDone() {
        if (number == 9) {
            if (isStrike()) {
                if (shotsFull() && thirdShot!=null)
                    return true;
                return false;
            }
        }
        if (isStrike() || isSpare() || shotsFull())
            return true;
        return false;
    }

    public void computeScore() {
        setScore(firstShot + secondShot);
    }


    public void computeScore(Integer score) {
        if (isStrike() || isSpare()) {
            setScore(score + 10);
        } else {

            if (firstShot != null && secondShot != null) {
                setScore(score + firstShot + secondShot);
            }
        }
    }
}
