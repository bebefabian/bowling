package com.jobsity.challenge.reader;

import com.jobsity.challenge.dto.PlayerInputScore;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BowlingFileReader implements GameFileReader {

    private BufferedReader objReader;

    @Override
    public List<PlayerInputScore> read(String inputFile) {
        List<PlayerInputScore> playerInputScores = new ArrayList<>();
        PlayerInputScore playerInputScore;

        try {

            objReader = new BufferedReader(new FileReader(inputFile));
            String strCurrentLine;

            while ((strCurrentLine = objReader.readLine()) != null) {
                playerInputScore = new PlayerInputScore();
                String[] input = strCurrentLine.split(" ");
                String name = input[0];
                String score = input[1];
                if(!Arrays.asList("F","0","1","2","3","4","5","6","7","8","9","10").contains(score)){
                    System.err.println("Invalid option:" + name + " " + score);
                }else{
                    playerInputScore.setName(input[0]);
                    playerInputScore.setScore(input[1]);
                    playerInputScores.add(playerInputScore);

                }
            }

            return playerInputScores;

        } catch (FileNotFoundException e) {
            throw new RuntimeException("File not found");
        } catch (IOException e) {
            throw new RuntimeException("Error reading the file");
        }
    }
}
