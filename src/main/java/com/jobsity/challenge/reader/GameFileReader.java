package com.jobsity.challenge.reader;

import com.jobsity.challenge.dto.PlayerInputScore;

import java.util.List;

public interface GameFileReader {

    public List<PlayerInputScore> read(String inputFile);
}
