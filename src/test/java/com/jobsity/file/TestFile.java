package com.jobsity.file;

import com.jobsity.challenge.dto.PlayerInputScore;
import com.jobsity.challenge.reader.BowlingFileReader;
import com.jobsity.challenge.reader.GameFileReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestFile {

    private GameFileReader gameFileReader;
    private String filename;

    private String FILE_1_INPUT = "src/test/resources/examples/example1.txt";

    @Before
    public void setup(){
        gameFileReader = new BowlingFileReader();
        this.filename = "temp";
    }

    @Test
    public void testFileReaderDontReturnNull() {
        List<PlayerInputScore> inputScores = gameFileReader.read(FILE_1_INPUT);
        Assert.assertNotNull(inputScores);
    }

    @Test
    public void testReaderReadsAtLeatOneLine() {
        List<PlayerInputScore> inputScores = gameFileReader.read(FILE_1_INPUT);
        Assert.assertTrue(inputScores.size()>0);
    }

    @Test
    public void testReaderMatchName(){
        String name = "Jose";
        List<PlayerInputScore> inputScores = gameFileReader.read(FILE_1_INPUT);
        Assert.assertEquals(name,inputScores.get(0).getName());
    }


    @Test
    public void testReaderMatchScore(){
        String score = "10";
        List<PlayerInputScore> inputScores = gameFileReader.read(FILE_1_INPUT);
        Assert.assertEquals(score,inputScores.get(0).getScore());
    }





}
