package com.jobsity.game;

import com.jobsity.challenge.game.framehandler.BowlingFrameHandler;
import com.jobsity.challenge.game.framehandler.FrameHandler;
import com.jobsity.challenge.model.Frame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FrameHandlerAddShots {

    private FrameHandler frameHandler;

    @Before
    public void setup(){
        this.frameHandler = new BowlingFrameHandler();
    }

    @Test
    public void addTwoShot(){
        List<Frame> frames = frameHandler.addShot(new ArrayList<>(),"5");
        frameHandler.addShot(frames,String.valueOf(4));
        Assert.assertTrue(frames.size()==1);
    }

    @Test
    public void addShotAndStrike(){
        List<Frame> frames = new ArrayList<>();
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"7");
//        frameHandler.addShot(frames,10);
//        frameHandler.addShot(frames,3);
//        frameHandler.addShot(frames,4);
//        frameHandler.addShot(frames,4);

        Assert.assertTrue(frames.size()==2);
    }

    @Test
    public void addMoreThan12Shots(){
        List<Frame> frames = new ArrayList<>();
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");
        frameHandler.addShot(frames,"10");




        Assert.assertTrue(frames.size()==10);
    }

}
