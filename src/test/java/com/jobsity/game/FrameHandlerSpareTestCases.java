package com.jobsity.game;

import com.jobsity.challenge.game.framehandler.BowlingFrameHandler;
import com.jobsity.challenge.model.Frame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class FrameHandlerSpareTestCases {

    private BowlingFrameHandler frameHanlder;



    @Before
    public void setup(){
        frameHanlder = new BowlingFrameHandler();
    }

    @Test
    public void testOnlyOneFrameWithNoSparkOrStrike(){
        Frame frame1 = new Frame(0);
        frame1.addShot("4");
        frame1.addShot("4");


        List<Frame>  frames = Arrays.asList(frame1);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(frames.get(0).getScore(),new Integer(8));
    }


    @Test
    public void testAddShot(){
        Frame frame1 = new Frame(0);
        frame1.addShot("4");
        frame1.addShot("4");

        Frame frame2 = new Frame(1);
        frame2.addShot("4");
        frame2.addShot("4");
        List<Frame>  frames = Arrays.asList(frame1,frame2);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(frames.get(1).getScore(),new Integer(16));
    }

    @Test
    public void testThirdFrameWithNoSpareOrStrike(){
        Frame frame1 = new Frame(0);
        frame1.addShot("4");
        frame1.addShot("4");

        Frame frame2 = new Frame(1);
        frame2.addShot("4");
        frame2.addShot("4");

        Frame frame3 = new Frame(2);
        frame3.addShot("4");
        frame3.addShot("4");
        List<Frame>  frames = Arrays.asList(frame1,frame2,frame3);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(frames.get(2).getScore(),new Integer(24));
    }

    @Test
    public void testSparkOnFirstFrame(){
        Frame frame1 = new Frame(0);
        frame1.addShot("5");
        frame1.addShot("5");


        List<Frame>  frames = Arrays.asList(frame1);
        frames = frameHanlder.computeScore(frames);
        Assert.assertNull(frames.get(0).getScore());
    }

    @Test
    public void testSparkOnSecondFirstFrameAndTwoShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot("4");
        frame1.addShot("4");

        Frame frame2 = new Frame(1);
        frame2.addShot("4");
        frame2.addShot("6");

        Frame frame3 = new Frame(2);
        frame3.addShot("3");

        List<Frame>  frames = Arrays.asList(frame1,frame2,frame3);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(frames.get(1).getScore(),new Integer(21));
    }

    @Test
    public void testSecondSparkWithNoFurtherShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot("4");
        frame1.addShot("4");

        Frame frame2 = new Frame(1);
        frame2.addShot("4");
        frame2.addShot("6");

        Frame frame3 = new Frame(1);
        frame3.addShot("3");
        frame3.addShot("4");

        List<Frame>  frames = Arrays.asList(frame1,frame2,frame3);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(frames.get(2).getScore(),new Integer(28));
    }

    @Test
    public void testFrstFrameSpareThenStrike(){
        Frame frame1 = new Frame(0);
        frame1.addShot("5");
        frame1.addShot("5");

        Frame frame2 = new Frame(1);
        frame2.addShot("10");

        List<Frame>  frames = Arrays.asList(frame1,frame2);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(20),frames.get(0).getScore());
    }
}
