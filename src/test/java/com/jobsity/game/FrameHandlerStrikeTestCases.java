package com.jobsity.game;

import com.jobsity.challenge.game.framehandler.BowlingFrameHandler;
import com.jobsity.challenge.model.Frame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class FrameHandlerStrikeTestCases {

    private BowlingFrameHandler frameHanlder;


    @Before
    public void setup(){
        frameHanlder = new BowlingFrameHandler();
    }

    @Test
    public void addStrikeFirstShot(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(5));
        frame2.addShot(String.valueOf(5));

        List<Frame> frames = Arrays.asList(frame1,frame2);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(20),frames.get(0).getScore());

    }

    @Test
    public void addStrikeaThenOnlyFirstShopt(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(5));

        List<Frame> frames = Arrays.asList(frame1,frame2);
        frames = frameHanlder.computeScore(frames);
        Assert.assertNull(frames.get(0).getScore());
    }

    @Test
    public void testFrstFrameAddStrikeThenTwoShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(5));
        frame2.addShot(String.valueOf(4));

        List<Frame> frames = Arrays.asList(frame1,frame2);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(19),frames.get(0).getScore());
    }

    @Test
    public void testSecondFrameAddStrikeThenTwoShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(5));
        frame2.addShot(String.valueOf(4));

        List<Frame> frames = Arrays.asList(frame1,frame2);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(28),frames.get(1).getScore());
    }


    @Test
    public void testTwoStrikeThenShot(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(10));

        Frame frame3 = new Frame(2);
        frame3.addShot(String.valueOf(5));

        List<Frame> frames = Arrays.asList(frame1,frame2,frame3);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(25),frames.get(0).getScore());
    }

    @Test
    public void testTwoStrikeThenStrike(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(10));

        Frame frame3 = new Frame(2);
        frame3.addShot(String.valueOf(10));

        List<Frame> frames = Arrays.asList(frame1,frame2,frame3);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(30),frames.get(0).getScore());
    }

    @Test
    public void testTwoStrikeThenTwoShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(10));

        Frame frame3 = new Frame(2);
        frame3.addShot(String.valueOf(10));

        Frame frame4 = new Frame(3);
        frame4.addShot(String.valueOf(5));

        List<Frame> frames = Arrays.asList(frame1,frame2,frame3,frame4);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(55),frames.get(1).getScore());
    }

    @Test
    public void testTwoStrikeThenThreeShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(10));

        Frame frame3 = new Frame(2);
        frame3.addShot(String.valueOf(10));

        Frame frame4 = new Frame(3);
        frame4.addShot(String.valueOf(5));
        frame4.addShot(String.valueOf(3));

        List<Frame> frames = Arrays.asList(frame1,frame2,frame3,frame4);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(73),frames.get(2).getScore());
    }

    @Test
    public void testForthFrameWithStrikeAndTwoShots(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(10));

        Frame frame3 = new Frame(2);
        frame3.addShot(String.valueOf(10));

        Frame frame4 = new Frame(3);
        frame4.addShot(String.valueOf(5));
        frame4.addShot(String.valueOf(3));

        List<Frame> frames = Arrays.asList(frame1,frame2,frame3,frame4);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(81),frames.get(3).getScore());
    }

    @Test
    public void testFiveStrike(){
        Frame frame1 = new Frame(0);
        frame1.addShot(String.valueOf(10));

        Frame frame2 = new Frame(1);
        frame2.addShot(String.valueOf(10));

        Frame frame3 = new Frame(2);
        frame3.addShot(String.valueOf(10));

        Frame frame4 = new Frame(3);
        frame4.addShot(String.valueOf(10));

        Frame frame5 = new Frame(4);
        frame5.addShot(String.valueOf(10));

        List<Frame> frames = Arrays.asList(frame1,frame2,frame3,frame4,frame5);
        frames = frameHanlder.computeScore(frames);
        Assert.assertEquals(new Integer(90),frames.get(2).getScore());
    }







}
