package com.jobsity.game;

import com.jobsity.challenge.model.Frame;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FrameTest {

    @Test
    public void testAddFirstShot(){
        String shot = "1";
        Frame frame = new Frame(1);
        frame.addShot(shot);
        Assert.assertEquals(new Integer(1),frame.getFirstShot());
    }

    @Test
    public void testAddSecondShot(){
        String shot1 = "1";
        String shot2 = "1";
        Frame frame = new Frame(0);
        frame.addShot(shot1);
        frame.addShot(shot2);
        Assert.assertEquals(new Integer(1),frame.getSecondShot());
    }

    @Test
    public void testScoreMatchNotSpareStrike(){
        String shot1 = "1";
        String shot2 = "1";
        Integer result = 2;
        Frame frame = new Frame(1);
        frame.addShot(shot1);
        frame.addShot(shot2);
        frame.computeScore(0);
        Assert.assertEquals(result,frame.getScore());
    }

    @Test
    public void testScoreMatcWithScore(){
        Integer shot1 = 1;
        Integer shot2 = 1;
        Integer score = 10;
        Integer result = 1+1+10;
        Frame frame = new Frame(1);
        frame.addShot(String.valueOf(shot1));
        frame.addShot(String.valueOf(shot2));
        frame.computeScore(score);
        Assert.assertEquals(result,frame.getScore());
    }


    @Test
    public void testShotIsStrike(){
        Integer shot = 10;
        Frame frame = new Frame(1);
        frame.addShot(String.valueOf(shot));
        Assert.assertTrue(frame.isStrike());
    }

    @Test
    public void testShotIsSpare(){
        String shot1 = "5";
        String shot2 = "5";
        Frame frame = new Frame(1);
        frame.addShot(shot1);
        frame.addShot(shot2);
        Assert.assertTrue(frame.isSpare());
    }

    @Test
    public void test(){
        List<Frame> frames = new ArrayList<>();
        Frame frame = new Frame(5);
        frame.addShot("2");
        frames.add(frame);
        frames.get(0).setSecondShot(4);

        Assert.assertEquals(new Integer(4),frame.getSecondShot());






    }
}
